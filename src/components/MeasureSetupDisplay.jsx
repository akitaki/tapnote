import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, createStyles } from '@material-ui/core/styles';
import {
  Slider,
  TextField,
  Zoom,
  FormControlLabel,
  Switch,
  Grid,
  Button,
  CardActions,
  Card,
  CardContent,
  Typography,
} from '@material-ui/core';
import TouchRipple from '@material-ui/core/ButtonBase/TouchRipple';

class MeasureSetupDisplay extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      // Timestamp (in ms) of the last tap
      lastTapTime: 0,
      // Number of taps since last reset
      numberOfTaps: 0,
      // Average period so far (in ms)
      avgPeriod: 0,
      manualBpm: false,
      manualBpmValue: 124,
    };

    this.bpmTouchRipple = React.createRef();
  }

  render() {
    // TODO: Split this giant render function into pieces

    const { manualBpm, manualBpmValue } = this.state;
    const { classes, timeSig } = this.props;
    const bpm = this.getBpm();
    const bpmTouchRipple = this.bpmTouchRipple;
    const allowedTimeSigs = [
      {
        value: 2,
        label: '2',
      },
      {
        value: 3,
        label: '3',
      },
      {
        value: 4,
        label: '4',
      },
      {
        value: 6,
        label: '6',
      },
    ];

    return (
      <div>
        <Grid container spacing={3}>
          {/* The Bpm element */}
          <Grid item xs={12} md={6}>
            <Card className={classes.card}>
              {/* Div for ripple tap effect */}
              <div
                type="button"
                onMouseDown={this.handleBpmMouseDown}
                onMouseUp={this.handleBpmMouseUp}
                style={{ position: 'relative' }}
                id="rippleDiv"
              >
                <CardContent>
                  <Typography
                    className={[classes.cardTitle, classes.noSelect]}
                    color="textSecondary"
                    gutterBottom
                  >
                    BPM - Press <kbd>space</kbd> / click me to the beat
                  </Typography>
                  {manualBpm || (
                    <Typography
                      variant="h4"
                      gutterBottom
                      align="center"
                      className={classes.noSelect}
                    >
                      {bpm.toFixed(0)}
                    </Typography>
                  )}
                  {manualBpm && (
                    <TextField
                      error={isNaN(manualBpmValue)}
                      onChange={this.handleBpmTextfieldChange}
                      fullWidth={true}
                      variant="outlined"
                      label="bpm"
                      type="number"
                      defaultValue="124"
                    />
                  )}
                </CardContent>
                <CardActions>
                  <Grid container justify="space-between">
                    <FormControlLabel
                      control={
                        <Switch
                          onMouseDown={this.stopPropagation}
                          onMouseUp={this.stopPropagation}
                          checked={manualBpm}
                          onChange={this.handleManualBpmChange}
                          color="primary"
                        />
                      }
                      label="Set manually"
                    />
                    {manualBpm || (
                      <Zoom in={!manualBpm}>
                        <Button
                          size="medium"
                          onClick={this.handleBpmResetClick}
                          // Prevent bubbling up to rippleDiv
                          onMouseDown={this.stopPropagation}
                          onMouseUp={this.stopPropagation}
                        >
                          Reset
                        </Button>
                      </Zoom>
                    )}
                  </Grid>
                </CardActions>
                <TouchRipple ref={bpmTouchRipple} center={false} />
              </div>
            </Card>
          </Grid>
          {/* The measure setup element */}
          <Grid item xs={12} md={6}>
            <Card className={classes.card}>
              <CardContent>
                <Typography className={classes.cardTitle} color="textSecondary" gutterBottom>
                  Beats per measure
                </Typography>
                <Slider
                  defaultValue={timeSig}
                  aria-labelledby="discrete-slider"
                  valueLabelDisplay="auto"
                  step={null}
                  marks={allowedTimeSigs}
                  onChangeCommitted={this.handleTimesigChange}
                  min={2}
                  max={6}
                />
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </div>
    );
  }

  // Add / remove keyboard event listeners
  componentDidMount() {
    document.addEventListener('keydown', this.handleKeyDown);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyDown);
  }

  getBpm = () => {
    const { manualBpm, manualBpmValue, avgPeriod } = this.state;
    if (manualBpm) {
      return manualBpmValue;
    } else {
      if (avgPeriod === 0) {
        return 0;
      } else {
        return 60 / (avgPeriod * 0.001);
      }
    }
  };

  /**
   * Calculate new period after taking `thisTapTime` into account.
   * @param {object} state
   * @param {number} thisTapTime
   */
  nextAvgPeriod = (state, thisTapTime) => {
    const { lastTapTime, numberOfTaps, avgPeriod } = state;

    if (lastTapTime === 0) {
      return {
        lastTapTime: +new Date(),
      };
    } else {
      const newNumberOfTaps = numberOfTaps + 1;

      // Rolling average algorithm
      const newAvgPeriod =
        avgPeriod * ((newNumberOfTaps - 1) / newNumberOfTaps) +
        (thisTapTime - lastTapTime) / newNumberOfTaps;

      return {
        lastTapTime: thisTapTime,
        numberOfTaps: newNumberOfTaps,
        avgPeriod: newAvgPeriod,
      };
    }
  };

  /**
   * Trigger ripple effect from the center of the bpm `Card` element.
   *
   * If the `e` is supplied, it will be passed to TouchRipple.start as is, and the ripple effect
   * will originate from the clicked position.
   * If the `e` is not supplied, the ripple will start from the center instead.
   *
   * If bpm input method is set to manual input, then this function does nothing.
   * @param {React.SyntheticEvent|undefined} e
   */
  triggerBpmRipple = (e) => {
    const { manualBpm } = this.state;
    if (manualBpm) {
      return;
    }

    const bpmTouchRipple = this.bpmTouchRipple;
    if (e) {
      bpmTouchRipple.current.start(e);
    } else {
      bpmTouchRipple.current.start();
      setTimeout(() => {
        bpmTouchRipple.current.stop({});
      }, 50);
    }
  };

  /**
   * Handle keydown effect (i.e. listen to spacebar press events)
   * @param {KeyboardEvent} event
   */
  handleKeyDown = (event) => {
    if (event.code === 'Space') {
      console.debug('Space pressed', event);

      // The setState() callback may be delayed, so it's better to get the timestamp here.
      const thisTapTime = +new Date();

      this.triggerBpmRipple();

      // The order of setState calls seems to be preserved:
      // https://stackoverflow.com/a/48610973
      this.setState(
        (state) => {
          return this.nextAvgPeriod(state, thisTapTime);
        },
        () => {
          const { onBpmUpdate } = this.props;
          if (onBpmUpdate) {
            onBpmUpdate(this.getBpm());
          }
        }
      );
    }
  };

  /**
   * Handle when the bpm `Card` element is clicked
   * @param {React.SyntheticEvent} e
   */
  handleBpmMouseDown = (e) => {
    console.debug('BPM clicked', e);

    this.triggerBpmRipple(e);

    const thisTapTime = +new Date();
    this.setState(
      (state) => {
        return this.nextAvgPeriod(state, thisTapTime);
      },
      () => {
        const { onBpmUpdate } = this.props;
        if (onBpmUpdate) {
          onBpmUpdate(this.getBpm());
        }
      }
    );
  };

  /**
   * Handle when the bpm `Card` element has been clicked
   * @param {React.SyntheticEvent} e
   */
  handleBpmMouseUp = (e) => {
    const bpmTouchRipple = this.bpmTouchRipple;
    bpmTouchRipple.current.stop(e);
  };

  handleBpmResetClick = (e) => {
    e.preventDefault();

    this.setState(
      {
        lastTapTime: 0,
        numberOfTaps: 0,
        avgPeriod: 0,
      },
      () => {
        const { onBpmUpdate } = this.props;
        if (onBpmUpdate) {
          onBpmUpdate(this.getBpm());
        }
      }
    );
  };

  /**
   * Stop bubbling of the event
   * @param {React.SyntheticEvent} e
   */
  stopPropagation = (e) => {
    e.stopPropagation();
  };

  handleManualBpmChange = (e) => {
    this.setState(
      {
        manualBpm: e.target.checked,
      },
      () => {
        const { onBpmUpdate } = this.props;
        if (onBpmUpdate) {
          onBpmUpdate(this.getBpm());
        }
      }
    );
  };

  handleBpmTextfieldChange = (e) => {
    const bpm = parseInt(e.target.value);

    this.setState(
      {
        manualBpmValue: bpm,
      },
      () => {
        const { onBpmUpdate } = this.props;
        if (onBpmUpdate) {
          onBpmUpdate(this.getBpm());
        }
      }
    );
  };

  handleTimesigChange = (_e, value) => {
    const { onTimeSignatureUpdate } = this.props;
    if (onTimeSignatureUpdate) {
      onTimeSignatureUpdate(value);
    }
  };
}

MeasureSetupDisplay.propTypes = {
  onBpmUpdate: PropTypes.func,
  onTimeSignatureUpdate: PropTypes.func,
  timeSig: PropTypes.number,
};

const styles = createStyles({
  cardTitle: {
    fotnSize: 14,
  },
  card: {
    // Force cards to be of the same height
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  // Disable text selection
  noSelect: {
    userSelect: 'none',
  },
});
export default withStyles(styles)(MeasureSetupDisplay);
