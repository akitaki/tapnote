import React from 'react';
import PropTypes from 'prop-types';
import {
  Link,
  TextField,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
} from '@material-ui/core';

export default class CopyDialog extends React.Component {
  render() {
    const { open, value, onClose } = this.props;
    return (
      <Dialog
        onClose={onClose}
        disableBackdropClick={false}
        aria-labelledby="simple-dialog-title"
        open={open}
      >
        <DialogTitle>Copy abc notation text</DialogTitle>
        <DialogContent>
          <DialogContentText>
            The text content is given in{' '}
            <Link href="https://en.wikipedia.org/wiki/ABC_notation">ABC notation</Link>. For
            Musescore, you may use the{' '}
            <Link href="https://musescore.org/en/project/abc-import">ABC import</Link> to import the
            notes.
          </DialogContentText>
          <TextField value={value} autoFocus onFocus={this.textFieldOnFocus} fullWidth={true} />
        </DialogContent>
      </Dialog>
    );
  }

  textFieldOnFocus = (event) => {
    event.target.select();
  };
}

CopyDialog.propTypes = {
  open: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
};
