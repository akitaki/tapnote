import React from 'react';
import PropTypes from 'prop-types';
import { Dialog, DialogTitle, List, ListItem, ListItemText, ListItemIcon } from '@material-ui/core';
import ImageIcon from '@material-ui/icons/Image';
import TextFieldsIcon from '@material-ui/icons/TextFields';

export default class ExportDialog extends React.Component {
  render() {
    const { open } = this.props;
    return (
      <Dialog
        onClose={this.handleDismiss}
        disableBackdropClick={false}
        aria-labelledby="simple-dialog-title"
        open={open}
      >
        <DialogTitle>Export as...</DialogTitle>
        <List>
          <ListItem
            button
            onClick={() => {
              this.handleClose('png');
            }}
            key="png"
          >
            <ListItemIcon>
              <ImageIcon />
            </ListItemIcon>
            <ListItemText>Download .png Image</ListItemText>
          </ListItem>
          <ListItem
            button
            onClick={() => {
              this.handleClose('abc');
            }}
            key="abc"
          >
            <ListItemIcon>
              <TextFieldsIcon />
            </ListItemIcon>
            <ListItemText>Copy ABC notation</ListItemText>
          </ListItem>
        </List>
      </Dialog>
    );
  }

  handleDismiss = () => {
    const { onClose } = this.props;
    if (onClose) {
      onClose();
    }
  };

  handleClose = (value) => {
    const { onClose } = this.props;
    if (onClose) {
      onClose(value);
    }
  };
}

ExportDialog.propTypes = {
  open: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
};
